package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

// Article to save article
type Article struct {
	Title   string `json:"title"`
	Desc    string `json:"desc"`
	Content string `json:"content"`
}

type Context struct {
	Db      *sql.DB
	Writer  http.ResponseWriter
	Request *http.Request
}

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Subject struct {
	Id             int    `json:"id"`
	Name           string `json:"name"`
	Description    string `json:"description"`
	MaxStudent     int    `json:"max_student"`
	CurrentStudent int    `json:"current_student"`
}

type Progress struct {
	UserId      int    `json:"user_id"`
	SubjectId   int    `json:"subject_id"`
	UserName    string `json:"user_name"`
	SubjectName string `json:"subject_name"`
	Status      string `json:"status"`
}

// Articles list of all article
var (
	mutex = &sync.Mutex{}
)

func main() {
	var port int
	flag.IntVar(&port, "port", 0, "port to serve")
	flag.Parse()

	if port == 0 {
		log.Fatal("Please provide the appropriate port")
	}

	db, err := sql.Open("mysql", "admin:admin123@tcp(127.0.0.1:3306)/test")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	ctx := Context{
		Db: db,
	}

	handle(port, ctx)
}

func handle(port int, ctx Context) {
	fmt.Printf("Server run at port %d\n", port)

	http.HandleFunc("/users", func(w http.ResponseWriter, r *http.Request) {
		ctx.Writer = w
		ctx.Request = r
		getAllUsers(ctx)
	})
	http.HandleFunc("/subjects", func(w http.ResponseWriter, r *http.Request) {
		ctx.Writer = w
		ctx.Request = r
		getAllSubjects(ctx)
	})
	http.HandleFunc("/progress", func(w http.ResponseWriter, r *http.Request) {
		ctx.Writer = w
		ctx.Request = r
		getProgress(ctx)
	})
	http.HandleFunc("/apply", func(w http.ResponseWriter, r *http.Request) {
		ctx.Writer = w
		ctx.Request = r
		applySubject(ctx)
	})
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))

}

func getAllUsers(ctx Context) {
	var users []User
	fmt.Println("Endpoint Hit: Get All Users")
	results, err := ctx.Db.Query("SELECT * FROM user")
	if err != nil {
		log.Fatal(err)
	}

	for results.Next() {
		var user User
		err = results.Scan(&user.Id, &user.Name)
		if err != nil {
			log.Fatal(err)
		}

		users = append(users, user)
	}

	json.NewEncoder(ctx.Writer).Encode(users)
}

func getAllSubjects(ctx Context) {
	var subjects []Subject
	fmt.Println("Endpoint Hit: Get All Subjects")
	results, err := ctx.Db.Query("SELECT * FROM subject")
	if err != nil {
		log.Fatal(err)
	}

	for results.Next() {
		var subject Subject
		err = results.Scan(&subject.Id, &subject.Name, &subject.Description, &subject.MaxStudent, &subject.CurrentStudent)
		if err != nil {
			log.Fatal(err)
		}

		subjects = append(subjects, subject)
	}

	json.NewEncoder(ctx.Writer).Encode(subjects)
}

func getProgress(ctx Context) {
	var progress []Progress
	fmt.Println("Endpoint Hit: Get All Progress")
	results, err := ctx.Db.Query("SELECT user_id, subject_id, user.name, subject.name, status FROM course JOIN user ON user.id = course.user_id JOIN subject ON subject.id = course.subject_id")
	if err != nil {
		log.Fatal(err)
	}

	for results.Next() {
		var prog Progress
		err = results.Scan(&prog.UserId, &prog.SubjectId, &prog.UserName, &prog.SubjectName, &prog.Status)
		if err != nil {
			log.Fatal(err)
		}

		progress = append(progress, prog)
	}

	json.NewEncoder(ctx.Writer).Encode(progress)
}

func applySubject(ctx Context) {
	var subject Subject

	fmt.Println("Endpoint Hit: Apply Subject")

	cont := ctx.Request.Context()
	keys := ctx.Request.URL.Query()
	userId := keys.Get("user_id")
	subjectId := keys.Get("subject_id")
	if userId == "" || subjectId == "" {
		log.Fatal("parameter missing")
		return
	}

	tx, err := ctx.Db.BeginTx(cont, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		log.Fatal(err)
		return
	}

	result := tx.QueryRow("SELECT * FROM subject where id = ?", subjectId)
	err = result.Scan(&subject.Id, &subject.Name, &subject.Description, &subject.MaxStudent, &subject.CurrentStudent)
	if err != nil {
		log.Fatal(err)
		tx.Rollback()
		return
	}

	if subject.CurrentStudent+1 <= subject.MaxStudent {
		subject.CurrentStudent++

		_, err = tx.Exec("INSERT INTO course (user_id, subject_id, status) VALUES (?, ?, ?)", userId, subjectId, "PROGRESS")
		if err != nil {
			log.Fatal(err)
			tx.Rollback()
			return
		}

		_, err = tx.Exec("UPDATE subject SET current_student = ? WHERE id = ?", subject.CurrentStudent, subjectId)
		if err != nil {
			log.Fatal(err)
			tx.Rollback()
			return
		}

		fmt.Fprintf(ctx.Writer, "Success apply user to subject")
	} else {
		fmt.Println("maximum student!")
		fmt.Fprintf(ctx.Writer, "Subject is already at maximum student!, Please select different Subject")
		return
	}

	tx.Commit()
}
