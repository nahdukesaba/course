# Simple Course

This is a simple app that store the subjects taken by students and their progress on said subjects. This app provides REST API call to list users, subjects the progress and apply user to a subject.

# API


## List all users

This API uses GET method and used to list all users
```
localhost:3031/users
```

## List all subjects

This API uses GET method and used to list all users
```
localhost:3031/subjects
```


## List of progress

This API uses GET method and used to list the progress of all users on each subject taken
```
localhost:3031/progress
```

## Apply user to subject

This API uses GET method and used to apply user to subject
```
localhost:3030/apply?user_id=1&subject_id=1
```

# How To Run

This app accept a flag to simplified the port setting.
```
go run main.go -port=3031 
```

# Other Additional
This app can also be paired up with another load balancer app to make sure the app can withstand massive API call at the same time.
A simple and easy to use load balancer can be found here:
> github.com/kasvith/simplelb